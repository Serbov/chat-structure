<?php
/**
 * User: Alex Serbov
 * Date: 13.07.2017
 * Time: 14:53
 */

require_once __DIR__ . '/Message.php';


class ConversationService
{
    private $userId;
    private $conversationId;
    private $amountMessage;
    // bd connection
    private $dbh;

    private $currentPageToken = null;
    private $lastReceivedMessage = null;

    /**
     * ChatService constructor.
     * @param $userId
     * @param $conversationId
     */
    public function __construct($dbh, $userId, $conversationId, $amountMessage = 10)
    {
        $this->dbh = $dbh;
        $this->userId = $userId;
        $this->conversationId = $conversationId;
        $this->amountMessage = $amountMessage;
    }

    /**
     * @return null
     */
    public function getCurrentPageToken()
    {
        return $this->currentPageToken;
    }

    /**
     * @return null
     */
    public function getLastReceivedMessage()
    {
        return $this->lastReceivedMessage;
    }


    public function getMessageHistory($pageToken = null)
    {
        if($pageToken === null) {
            $sth = $this->dbh->prepare('SELECT id FROM `message` ORDER BY id DESC LIMIT 1');
            $sth->execute();
            $pageToken = $sth->fetchColumn() + 1;
        }

        $sth = $this->dbh->prepare('call GET_MESSAGE_HISTORY(:userId, :conversationId, :pageToken, :amountMessage)');

        $sth->bindValue(':userId', $this->userId, PDO::PARAM_INT);
        $sth->bindValue(':conversationId', $this->conversationId, PDO::PARAM_INT);
        $sth->bindValue(':amountMessage', $this->amountMessage, PDO::PARAM_INT);
        $sth->bindValue(':pageToken', $pageToken, PDO::PARAM_INT);
        $sth->execute();

        $messages = $sth->fetchAll(PDO::FETCH_CLASS, Message::class);

        $this->currentPageToken = empty($messages) ? $pageToken : end($messages)->getId();

        return $messages;
    }


    public function getNewMessages($lastReceivedMessage = null)
    {
        $sth = $this->dbh->prepare('SELECT m.* 
              FROM `message` m
              LEFT JOIN `delete_message` dm 
              ON m.id = dm.message_id AND dm.user_id = :userId
              WHERE m.conversation_id = :conversationId
              AND m.id > :lastReceivedMessage
              AND dm.id is NULL
              ORDER BY m.id');

        $sth->bindValue(':userId', $this->userId, PDO::PARAM_INT);
        $sth->bindValue(':conversationId', $this->conversationId, PDO::PARAM_INT);
        $sth->bindValue(':lastReceivedMessage', $lastReceivedMessage, PDO::PARAM_INT);
        $sth->execute();

        $messages = $sth->fetchAll(PDO::FETCH_CLASS, Message::class);

        $this->lastReceivedMessage = empty($messages) ? $lastReceivedMessage : reset($messages)->getId();

        return $messages;
    }
    // если ли у пользователя доступ к беседе
    public function isAccessToConversation()
    {
        $sth = $this->dbh->prepare('SELECT *
              FROM `conversation_member`
              WHERE conversation_id = :conversationId
              AND user_id = :userId');

        $sth->bindValue(':userId', $this->userId, PDO::PARAM_INT);
        $sth->bindValue(':conversationId', $this->conversationId, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchColumn();

        return $result !== false;
    }

    public function deleteMessage($messageId)
    {
        if(empty($messageId))
            return false;

        $sth = $this->dbh->prepare('SELECT m.id 
              FROM `message` m
              LEFT JOIN `delete_message` dm 
              ON m.id = dm.message_id AND dm.user_id = :userId
              WHERE m.id = :messageId
              AND dm.id is NULL');
        $sth->bindValue(':userId', $this->userId, PDO::PARAM_INT);
        $sth->bindValue(':messageId', $messageId, PDO::PARAM_INT);
        $sth->execute();

        /** @var Message $message */
        $mId = $sth->fetchColumn();
        if(empty($mId))
            return false;

        $sth = $this->dbh->prepare("INSERT INTO `delete_message` (message_id, user_id) VALUES (:messageId, :userId)");
        $sth->bindValue(':userId', $this->userId, PDO::PARAM_INT);
        $sth->bindValue(':messageId',$mId, PDO::PARAM_INT);
        $sth->execute();

        return true;
    }

}