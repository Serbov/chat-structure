-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3308
-- Время создания: Июл 17 2017 г., 20:24
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `chat`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`%` PROCEDURE `GET_MESSAGE_HISTORY` (IN `userId` INT, IN `conversationId` INT, IN `pageToken` INT, IN `amountMessage` INT)  BEGIN
	Declare is_not_end_cursor INT default 1;
	Declare counter INT default 0;	
	
	Declare v_id INT;
	Declare v_parent_message_id INT;
	Declare v_is_embed tinyint;
	
	Declare v_prev_id INT default 0; 
	Declare v_prev_parent_message_id INT;
	Declare v_prev_is_embed tinyint;
    	
	Declare MessageCursor Cursor for SELECT m.id, m.parent_message_id, m.is_embed
	FROM message m
	LEFT JOIN delete_message dm 
	ON m.id = dm.message_id AND dm.user_id = userId
	WHERE m.conversation_id = conversationId	
	AND m.id < pageToken
	AND dm.id is NULL
	ORDER BY m.id DESC;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET is_not_end_cursor = 0;  

    DROP TEMPORARY TABLE IF EXISTS tmp_message;
    CREATE TEMPORARY TABLE tmp_message (
		id int(11) NOT NULL,
		parent_message_id int(11) DEFAULT NULL,
		is_embed tinyint(1) NOT NULL
	);
	
	open MessageCursor;

	FETCH MessageCursor INTO v_id, v_parent_message_id, v_is_embed;
	
	IF is_not_end_cursor != 0 THEN
		SET v_prev_id = v_id;
		SET v_prev_parent_message_id = v_parent_message_id;		
		SET v_prev_is_embed = v_is_embed;
		
		WHILE is_not_end_cursor AND counter < amountMessage DO

		FETCH MessageCursor INTO v_id, v_parent_message_id, v_is_embed;
		
		IF v_prev_is_embed = 1 THEN
			IF	(is_not_end_cursor = 0 AND v_prev_is_embed = 1) OR 
				(v_is_embed = 1 AND v_prev_parent_message_id != v_parent_message_id) OR  
				(v_is_embed = 0 AND v_prev_parent_message_id != v_id) THEN
				SET v_prev_is_embed = 0;
				SET counter = counter + 1;			
			END IF;
		ELSE
			SET counter = counter + 1;
		END IF;

		insert into tmp_message values (v_prev_id, v_prev_parent_message_id, v_prev_is_embed);

		SET v_prev_id = v_id;
		SET v_prev_parent_message_id = v_parent_message_id;		
		SET v_prev_is_embed = v_is_embed;		

		END WHILE;  
		
		SELECT m.id, m.user_id, m.conversation_id,
		m.parent_message_id, m.text, tm.is_embed, m.create_at
		FROM tmp_message tm JOIN message m ON tm.id = m.id;
		DROP TEMPORARY TABLE IF EXISTS tmp_message;
		
	END IF;
	CLOSE MessageCursor; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `conversation`
--

CREATE TABLE `conversation` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `conversation`
--

INSERT INTO `conversation` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Структура таблицы `conversation_member`
--

CREATE TABLE `conversation_member` (
  `user_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `conversation_member`
--

INSERT INTO `conversation_member` (`user_id`, `conversation_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `delete_message`
--

CREATE TABLE `delete_message` (
  `id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `delete_message`
--

INSERT INTO `delete_message` (`id`, `message_id`, `user_id`) VALUES
(1, 4, 1),
(2, 1, 1),
(8, 20, 1),
(9, 21, 1),
(10, 18, 1),
(11, 17, 1),
(12, 29, 1),
(13, 25, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `parent_message_id` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci,
  `is_embed` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `user_id`, `conversation_id`, `parent_message_id`, `text`, `is_embed`, `create_at`) VALUES
(1, 1, 1, NULL, 'test', 0, '2017-07-13 07:29:31'),
(2, 1, 1, 1, 'children', 1, '2017-07-13 07:22:23'),
(3, 2, 1, NULL, 'tessss', 0, '2017-07-13 08:24:23'),
(4, 1, 1, NULL, 'hrhy', 0, '2017-07-13 10:26:26'),
(5, 1, 1, 4, 'дочернеее письмо', 1, '2017-07-13 12:31:29'),
(6, 1, 1, 4, 'дочернеее письмо 4', 1, '2017-07-13 12:31:29'),
(7, 1, 1, NULL, 'родительское', 0, '2017-07-13 10:26:26'),
(8, 1, 1, NULL, 'родительское 1', 0, '2017-07-13 10:26:26'),
(9, 1, 1, NULL, 'родительское 2', 0, '2017-07-13 10:26:26'),
(10, 1, 1, NULL, 'родительское 4', 0, '2017-07-13 10:26:26'),
(11, 1, 1, NULL, 'родительское 5', 0, '2017-07-13 10:26:26'),
(12, 1, 1, NULL, 'родительское 6', 0, '2017-07-13 10:26:26'),
(13, 1, 1, NULL, 'родительское', 0, '2017-07-13 10:26:26'),
(14, 1, 1, NULL, 'родительское 1', 0, '2017-07-13 10:26:26'),
(15, 1, 1, NULL, 'родительское 2', 0, '2017-07-13 10:26:26'),
(16, 1, 1, NULL, 'родительское 4', 0, '2017-07-13 10:26:26'),
(17, 1, 1, NULL, 'родительское 5', 0, '2017-07-13 10:26:26'),
(18, 1, 1, NULL, 'родительское 6', 0, '2017-07-13 10:26:26'),
(19, 1, 1, NULL, 'родительское 2', 0, '2017-07-13 10:26:26'),
(20, 1, 1, NULL, 'родительское 4', 0, '2017-07-13 10:26:26'),
(21, 1, 1, NULL, 'родительское 5', 0, '2017-07-13 10:26:26'),
(22, 1, 1, NULL, 'родительское 6', 0, '2017-07-13 10:26:26'),
(23, 1, 1, NULL, 'родительское', 0, '2017-07-13 10:26:26'),
(24, 1, 1, NULL, 'родительское 1', 0, '2017-07-13 10:26:26'),
(25, 1, 1, NULL, 'родительское 2', 0, '2017-07-13 10:26:26'),
(26, 1, 1, NULL, 'родительское 4', 0, '2017-07-13 10:26:26'),
(27, 1, 1, NULL, 'родительское 5', 0, '2017-07-13 10:26:26'),
(28, 1, 1, NULL, 'родительское 6', 0, '2017-07-13 10:26:26'),
(29, 1, 1, NULL, 'tetttttttttttttttttttt', 0, '2017-07-13 10:26:26');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`) VALUES
(1),
(2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `conversation_member`
--
ALTER TABLE `conversation_member`
  ADD PRIMARY KEY (`user_id`,`conversation_id`),
  ADD KEY `IDX_422840B8A76ED395` (`user_id`),
  ADD KEY `IDX_422840B89AC0396` (`conversation_id`);

--
-- Индексы таблицы `delete_message`
--
ALTER TABLE `delete_message`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_154CE429537A1329` (`message_id`),
  ADD KEY `IDX_154CE429A76ED395` (`user_id`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FA76ED395` (`user_id`),
  ADD KEY `IDX_B6BD307F9AC0396` (`conversation_id`),
  ADD KEY `IDX_B6BD307F14399779` (`parent_message_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `delete_message`
--
ALTER TABLE `delete_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `conversation_member`
--
ALTER TABLE `conversation_member`
  ADD CONSTRAINT `FK_422840B89AC0396` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
  ADD CONSTRAINT `FK_422840B8A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `delete_message`
--
ALTER TABLE `delete_message`
  ADD CONSTRAINT `FK_154CE429537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_154CE429A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F14399779` FOREIGN KEY (`parent_message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_B6BD307F9AC0396` FOREIGN KEY (`conversation_id`) REFERENCES `conversation` (`id`),
  ADD CONSTRAINT `FK_B6BD307FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
