<?php
/**
 * User: Alex Serbov
 * Date: 13.07.2017
 * Time: 14:53
 */


class Message implements JsonSerializable
{
    private $id;
    private $user_id;
    private $conversation_id;
    private $parent_message_id;
    private $text;
    private $is_embed;
    private $create_at;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getConversationId()
    {
        return $this->conversation_id;
    }

    /**
     * @param mixed $conversation_id
     */
    public function setConversationId($conversation_id)
    {
        $this->conversation_id = $conversation_id;
    }

    /**
     * @return mixed
     */
    public function getParentMessageId()
    {
        return $this->parent_message_id;
    }

    /**
     * @param mixed $parent_message_id
     */
    public function setParentMessageId($parent_message_id)
    {
        $this->parent_message_id = $parent_message_id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function IsEmbed()
    {
        return $this->is_embed;
    }

    /**
     * @param mixed $is_embed
     */
    public function setEmbed($is_embed)
    {
        $this->is_embed = $is_embed;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
    }

    public function jsonSerialize() {

        return [
            'id' => $this->id,
            'user' => $this->user_id,
            'conversation' => $this->conversation_id,
            'text' => $this->text,
            'parent_message' => $this->parent_message_id,
            'embed' => (bool)$this->is_embed,
            'createAt' => $this->create_at
        ];
    }

}