<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 13.07.2017
 * Time: 19:58
 */
include __DIR__ . '/ConversationController.php';

$controller = new ConversationController();

switch ($_GET['route']) {
    case 'newMessage':
        $controller->getNewMessagesAction();
        break;
    case 'messageHistory':
        $controller->getMessageHistoryAction();
        break;
    case 'deleteMessage':
        $controller->deleteMessageAction();
        break;
    default:
        include __DIR__ . './main_template.html';
}