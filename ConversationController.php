<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 13.07.2017
 * Time: 19:34
 */
require_once __DIR__ . "/ConversationService.php";
require_once __DIR__ . "/Message.php";

class ConversationController
{
    private $conversationService;

    public function __construct()
    {
        try {
            $dbh = new PDO('mysql:dbname=chat;host=127.0.0.1;port=3308', 'root', '');
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }

        $this->conversationService = new ConversationService($dbh, 1, 1);

        if(!$this->conversationService->isAccessToConversation()) {
            die('access denied!');
        }
    }

    public function getMessageHistoryAction()
    {
        $pageToken = empty($_GET['pageToken']) ? null : $_GET['pageToken'];

        $messages = $this->conversationService->getMessageHistory($pageToken);

        $outData = ['messages' => $messages, 'pageToken' => $this->conversationService->getCurrentPageToken()];

        if(empty($pageToken))
            $outData['lastReceivedMessage'] = reset($messages)->getId();


        $response = json_encode($outData);
        header('Content-Type: application/json');
        echo $response;
    }

    public function getNewMessagesAction()
    {
        $lastReceivedMessage = empty($_GET['lastReceivedMessage']) ? null : $_GET['lastReceivedMessage'];

        $messages = $this->conversationService->getNewMessages($lastReceivedMessage);
        $lastReceivedMessage = $this->conversationService->getLastReceivedMessage();

        $response = json_encode(['messages' => $messages, 'lastReceivedMessage' => $lastReceivedMessage]);
        header('Content-Type: application/json');
        echo $response;
    }


    public function deleteMessageAction()
    {
        $messageId = empty($_GET['messageId']) ? null : $_GET['messageId'];
        $result = $this->conversationService->deleteMessage($messageId);
        $response = json_encode(['result' => $result]);
        header('Content-Type: application/json');
        echo $response;
    }

}